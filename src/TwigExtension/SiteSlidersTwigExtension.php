<?php

namespace Drupal\site_sliders\TwigExtension;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class SiteSlidersTwigExtension extends \Twig_Extension {

  /**
   * Generates a list of all Twig functions that this extension defines.
   *
   * @return array
   *   A key/value array that defines custom Twig functions. The key denotes the
   *   function name used in the tag, e.g.:
   *   @code
   *   {{ testfunc() }}
   *   @endcode
   *
   *   The value is a standard PHP callback that defines what the function does.
   */
  public function getFunctions() {
    return array(
      'getSlider' => new \Twig_Function_Function(array('Drupal\site_sliders\TwigExtension\SiteSlidersTwigExtension', 'getSlider')),
    );
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_sliders.twig_extension';
  }

  /**
   * Отображение слайдера по имени.
   */
  public static function getSlider($name) {
    return site_slider_get($name);
  }
}
