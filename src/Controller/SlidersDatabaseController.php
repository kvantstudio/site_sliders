<?php

/**
 * @file
 * Contains \Drupal\site_sliders\Controller\SlidersDatabaseController.
 */

namespace Drupal\site_sliders\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Database class.
 */
class SlidersDatabaseController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new SlidersDatabaseController.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Загружает тип слайдера.
   */
  public function loadSliderType($stid) {
    $query = $this->connection->select('site_sliders_types', 'n');
    $query->fields('n');
    $query->condition('n.stid', $stid);

    return $query->execute()->fetchObject();
  }

  /**
   *  Сохраняет тип слайдера в базе данных.
   */
  public function insertSliderType($entry) {
    $return_value = $this->connection->insert('site_sliders_types')
      ->fields($entry)
      ->execute();

    return $return_value;
  }

  /**
   *  Обновляет тип слайдера в базе данных.
   */
  public function updateSliderType($entry) {
    $return_value = $this->connection->update('site_sliders_types')
      ->fields($entry)
      ->condition('stid', $entry['stid'])
      ->execute();

    return $return_value;
  }

  /**
   * Формирует массив с именами и идентификаторами всех типов слайдеров.
   */
  public function loadSlidersTypes() {
    $query = $this->connection->select('site_sliders_types', 'n');
    $query->fields('n');
    $result = $query->execute();

    $sliders_types = [];

    foreach ($result as $row) {
      $sliders_types += array($row->stid => $row->title);
    }

    return $sliders_types;
  }

  /**
   * Загружает слайд.
   */
  public function loadSlide($sid) {
    $query = $this->connection->select('site_sliders', 'n');
    $query->fields('n');
    $query->condition('n.sid', $sid);

    return $query->execute()->fetchObject();
  }

  /**
   *  Сохраняет слайдер в базе данных.
   */
  public function insertSlide($entry) {
    $return_value = $this->connection->insert('site_sliders')
      ->fields($entry)
      ->execute();

    return $return_value;
  }

  /**
   *  Обновляет слайдер в базе данных.
   */
  public function updateSlide($entry) {
    $return_value = $this->connection->update('site_sliders')
      ->fields($entry)
      ->condition('sid', $entry['sid'])
      ->execute();

    return $return_value;
  }

  /**
   *  Удаляет слайд в базе данных с файлом.
   */
  public function deleteSlide($sid) {
    $return_value = NULL;

    // Изменяем применяемость файла.
    $file_usage = \Drupal::service('file.usage');

    if ($file_current_usage = $this->loadSlideFile('slide', $sid)) {
      $file_usage->delete($file_current_usage, 'site_sliders', 'slide', $sid);
      file_delete($file_current_usage->getFileUri());

      $return_value = $this->connection->delete('site_sliders')
        ->condition('sid', $sid)
        ->execute();
    }

    return $return_value;
  }

  /**
   * Загружает информацию о файле слайда.
   */
  public function loadSlideFile($type, $id) {
    $query = $this->connection->select('file_usage', 'n');
    $query->fields('n', array('fid'));
    $query->condition('n.module', 'site_sliders');
    $query->condition('n.type', $type);
    $query->condition('n.id', $id);
    $fid = $query->execute()->fetchField();

    $file = NULL;
    if ($fid) {
      $file = File::load($fid);
    }
    return $file;
  }

  /**
   *  Обновляет вес слайдов в базе данных.
   */
  public function slidesSetWeight($entry) {
    $return_value = $this->connection->update('site_sliders')
      ->fields($entry)
      ->condition('stid', $entry['stid'])
      ->condition('sid', $entry['sid'])
      ->execute();

    return $return_value;
  }
}