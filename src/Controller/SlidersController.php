<?php

/**
 * @file
 * Contains \Drupal\site_sliders\Controller\SlidersController
 */

namespace Drupal\site_sliders\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;

class SlidersController extends ControllerBase {

  /**
   * Страница отображения зарегистрированных слайдов.
   */
  public function getSliders() {
    return array(
      '#theme' => 'site_sliders',
      '#cache' => [
        'keys' => ['sliders', 'full'],
        'tags' => ['sliders'],
        'contexts' => ['languages', 'timezone'],
        'max-age' => Cache::PERMANENT,
      ],
    );
  }

}