<?php

/**
 * @file
 * Contains \Drupal\site_sliders\Controller\SlidersAjaxController
 */

namespace Drupal\site_sliders\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SlidersAjaxController extends ControllerBase {

  /**
   * The servises classes.
   *
   * @var \Drupal\site_sliders\Controller\SlidersDatabaseController
   */
  protected $databaseSliders;

  /**
   * Construct.
   *
   * @param \Drupal\site_sliders\Controller\SlidersDatabaseController $connection
   *   The database connection.
   */
  public function __construct(SlidersDatabaseController $databaseSliders) {
    $this->databaseSliders = $databaseSliders;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_sliders.database')
    );
  }

  /**
   * Регистрирует вес позиций в категории.
   *
   * @param  [string]   $method
   * @param  [int]      $cid
   * @param  [string]   $pids
   *
   * @return ajax response
   *
   * @access public
   */
  public function slidesSetWeight($method, $stid, $sorted) {
    // Create AJAX Response object.
    $response = new AjaxResponse();

    if ($method == 'ajax') {
      $sids = explode("&sid=", $sorted);
      unset($sids[0]);

      $weight = 1;
      foreach ($sids as $sid) {
        //$response->addCommand(new AlertCommand("Тип слайдера: " . $stid . "; Слайд: " . $sid . "; Номер: " . $weight));
        $data = array(
          'stid' => $stid,
          'sid' => $sid,
          'weight' => $weight,
        );
        $this->databaseSliders->slidesSetWeight($data);
        $weight++;
      }

      // Очищает cache.
      Cache::invalidateTags(['sliders']);
      Cache::invalidateTags(['slider-' . $stid]);

    }

    // Return ajax response.
    return $response;
  }
}