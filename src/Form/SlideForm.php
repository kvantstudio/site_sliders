<?php

/**
 * @file
 * Contains \Drupal\site_sliders\Form\SlideForm
 */

namespace Drupal\site_sliders\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\site_sliders\Controller\SlidersDatabaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Step class form.
 */
class SlideForm extends FormBase {

  /**
   * The servises classes.
   *
   * @var \Drupal\site_sliders\Controller\SlidersDatabaseController
   */
  protected $databaseSliders;

  /**
   * Constructs a new DblogClearLogForm.
   *
   * @param \Drupal\site_sliders\Controller\SlidersDatabaseController $connection
   *   The database connection.
   */
  public function __construct(SlidersDatabaseController $databaseSliders) {
    $this->databaseSliders = $databaseSliders;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_sliders.database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_slide_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $stid = 0, $sid = 0) {
    // Загружаем объекты если переданы идентификаторы.
    $slider_type = $this->databaseSliders->loadSliderType($stid);
    $slide = $this->databaseSliders->loadSlide($sid);

    // Объект тип слайда.
    $form['slider_type'] = array(
      '#type' => 'value',
      '#value' => $slider_type,
    );

    // Объект слайда.
    $form['slide'] = array(
      '#type' => 'value',
      '#value' => $slide,
    );

    // Изображение слайда.
    if (isset($slide->sid)) {
      $image = '';
      if ($file = $this->databaseSliders->loadSlideFile('slide', $sid)) {
        $uri = $file->getFileUri();
        $url = file_create_url($uri);
        $image = ImageStyle::load('large')->buildUrl($uri);
      }
      $form['image'] = array(
        '#type' => 'item',
        '#title' => $this->t('Image'),
        '#markup' => '<img src="' . $image . '" alt="' . $this->t('Image') . '">',
        '#access' => $image ? TRUE : FALSE,
      );
    }

    $form['stid'] = array(
      '#title' => $this->t('Slider'),
      '#type' => 'select',
      '#default_value' => isset($slider_type->stid) ? $slider_type->stid : 0,
      '#description' => $this->t('Select type of slider.'),
      '#options' => $this->databaseSliders->loadSlidersTypes(),
      '#disabled' => isset($slider_type->stid) ? 1 : 0,
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#required' => FALSE,
      '#default_value' => isset($slide->sid) ? $slide->title : "",
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#required' => FALSE,
      '#default_value' => isset($slide->sid) ? $slide->description : "",
    );

    $form['title_link'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name link'),
      '#maxlength' => 255,
      '#required' => FALSE,
      '#default_value' => isset($slide->sid) ? $slide->title_link : "",
    );

    $form['nid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search page number by name'),
      '#autocomplete_route_name' => 'kvantstudio.search_node_autocomplete',
    ];

    if (\Drupal::moduleHandler()->moduleExists('site_commerce')) {
      $form['site_commerce'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Online store'),
        '#collapsible' => FALSE,
      );

      $form['site_commerce']['site_commerce_tid'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Search category page number by name'),
        '#autocomplete_route_name' => 'site_commerce.search_category_autocomplete',
      ];

      $form['site_commerce']['site_commerce_pid'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Search product page number by name'),
        '#autocomplete_route_name' => 'site_commerce.search_product_autocomplete',
      ];
    }

    $form['href'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Link URL'),
      '#maxlength' => 512,
      '#required' => FALSE,
      '#default_value' => isset($slide->href) ? $slide->href : "",
    );

    $form['file'] = array(
      '#title' => $this->t('File'),
      '#type' => 'file',
      '#description' => $this->t('Select image of slide.'),
      '#required' => FALSE,
      '#access' => TRUE,
    );

    // Now we add our submit button, for submitting the form results.
    // The 'actions' wrapper used here isn't strictly necessary for tabledrag,
    // but is included as a Form API recommended practice.
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Определяем папку.
    $directory = 'public://images/sliders/';

    // Загрузка файла.
    $form_state->setValue('uploaded_file', 0);
    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY) && !empty($_FILES['files']['name']['file'])) {
      // Массивы с валидаторами для файла изображения.
      $validators = array(
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('jpg gif png jpeg'),
        'file_validate_name_length' => array(),
        'file_validate_image_resolution' => array('1920x1080'),
        'file_validate_size' => array(5 * 1024 * 1024),
      );

      // Сохранить файл в качестве временного.
      $file = file_save_upload('file', $validators, $directory, FILE_EXISTS_RENAME);

      // Если файл был успешно загружен.
      if ($file) {
        // Получаем расширение файла.
        $image = \Drupal::service('image.factory')->get($file->getFileUri());
        $extension = image_type_to_extension($image->getToolkit()->getType(), false);

        // Переименовываем файл.
        if ($extension) {
          $new_filename = $file->uuid() . "." . $extension;
          $destination = file_create_filename($new_filename, $directory);
          $file = file_move($file, $destination);

          // Помечаем, что файл успешно был загружен.
          $form_state->setValue('uploaded_file', $file);
        } else {
          // Предупреждение о том, что произошла ошибка при загрузке файла.
          $form_state->setErrorByName(
            'file',
            $this->t('Failed to write the uploaded file.')
          );
        }
      } else {
        // Предупреждение о том, что произошла ошибка при загрузке файла.
        $form_state->setErrorByName(
          'file',
          $this->t('Failed to write the uploaded file.')
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $slide = $form_state->getValue('slide');

    $href = trim($form_state->getValue('href'));
    if (!$href) {
      $nid = (int) trim($form_state->getValue('nid'));
      if ($nid) {
        $href = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $nid);
      }

      $site_commerce_tid = (int) trim($form_state->getValue('site_commerce_tid'));
      if ($site_commerce_tid) {
        $href = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $site_commerce_tid);
      }

      $site_commerce_pid = (int) trim($form_state->getValue('site_commerce_pid'));
      if ($site_commerce_pid) {
        $href = \Drupal::service('path.alias_manager')->getAliasByPath('/site-commerce/' . $site_commerce_pid);
      }
    }

    // Выполняем если создаётся новый слайд.
    $return_created = null;
    $return_updated = null;
    if (!isset($slide->sid)) {
      $data = array(
        'stid' => $form_state->getValue('stid'),
        'title' => $form_state->getValue('title'),
        'description' => $form_state->getValue('description'),
        'title_link' => $form_state->getValue('title_link'),
        'href' => $href,
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'weight' => 0,
      );
      $return_created = $this->databaseSliders->insertSlide($data);
      $slide = $this->databaseSliders->loadSlide($return_created);
    } else {
      // Выполняем если обновляется слайд.
      $entry = array(
        'sid' => $slide->sid,
        'title' => $form_state->getValue('title'),
        'description' => $form_state->getValue('description'),
        'title_link' => $form_state->getValue('title_link'),
        'href' => $href,
        'changed' => REQUEST_TIME,
        'weight' => 0,
      );
      $return_updated = $this->databaseSliders->updateSlide($entry);
    }

    // Изменяем статус файла на постоянный.
    if ($file = $form_state->getValue('uploaded_file')) {
      $file->status = FILE_STATUS_PERMANENT;
      $file->save();

      // Изменяем применяемость файла.
      $file_usage = \Drupal::service('file.usage');

      if ($file_current_usage = $this->databaseSliders->loadSlideFile('slide', $slide->sid)) {
        $file_usage->delete($file_current_usage, 'site_sliders', 'slide', $slide->sid);
        file_delete($file_current_usage->getFileUri());
      }

      $file_usage->add($file, 'site_sliders', 'slide', $slide->sid);
    }

    if ($return_created) {
      drupal_set_message($this->t('Slide created.'));
    }

    if ($return_updated) {
      drupal_set_message($this->t('Slide updated.'));
    }

    // Очищает cache.
    Cache::invalidateTags(['sliders']);
  }
}
