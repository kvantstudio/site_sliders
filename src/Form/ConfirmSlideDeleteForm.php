<?php

/**
 * @file
 * Contains \Drupal\site_sliders\Form\ConfirmSlideDeleteForm.
 */

namespace Drupal\site_sliders\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\site_sliders\Controller\SlidersDatabaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to unban IP addresses.
 */
class ConfirmSlideDeleteForm extends ConfirmFormBase {

  /**
   * The ID of the parametrs.
   *
   * @var integral
   */
  protected $course_id;
  protected $course_sid;
  protected $course_eid;
  protected $sid;

  /**
   * The object to delete.
   *
   * @var object
   */
  protected $slide;

  /**
   * The servises classes.
   *
   * @var \Drupal\site_sliders\Controller\SlidersDatabaseController
   */
  protected $databaseSliders;

  /**
   * Constructs a new DblogClearLogForm.
   *
   * @param \Drupal\site_sliders\Controller\SlidersDatabaseController $connection
   *   The database connection.
   */
  public function __construct(SlidersDatabaseController $databaseSliders) {
    $this->databaseSliders = $databaseSliders;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_sliders.database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slide_delete_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param string $ban_id
   *   The IP address record ID to unban.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $sid = NULL) {
    $this->sid = (int) $sid;
    if ($this->sid) {
      $slide = $this->databaseSliders->loadSlide($sid);
      $this->slide = $slide;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete «@title»?', array('@title' => $this->slide->title));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('site_sliders.admin');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->databaseSliders->deleteSlide($this->slide->sid);

    // Очищает cache.
    Cache::invalidateTags(['sliders']);

    drupal_set_message($this->t('The slide was deleted.'));
    $form_state->setRedirect('site_sliders.admin');
  }
}