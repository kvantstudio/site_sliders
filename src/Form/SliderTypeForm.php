<?php

/**
 * @file
 * Contains \Drupal\site_sliders\Form\SliderTypeForm
 */

namespace Drupal\site_sliders\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_sliders\Controller\SlidersDatabaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Slider type class form.
 */
class SliderTypeForm extends FormBase {

  /**
   * The servises classes.
   *
   * @var \Drupal\site_sliders\Controller\SlidersDatabaseController
   */
  protected $databaseSliders;

  /**
   * Constructs a new DblogClearLogForm.
   *
   * @param \Drupal\site_sliders\Controller\SlidersDatabaseController $connection
   *   The database connection.
   */
  public function __construct(SlidersDatabaseController $databaseSliders) {
    $this->databaseSliders = $databaseSliders;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_sliders.database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_sliders_type_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $stid = 0) {
    // Загружаем объекты если переданы идентификаторы.
    $slider_type = $this->databaseSliders->loadSliderType($stid);

    // Объект тип слайда.
    $form['slider_type'] = array(
      '#type' => 'value',
      '#value' => $slider_type,
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#required' => true,
      '#default_value' => isset($slider_type->stid) ? $slider_type->title : "",
    );

    $form['image_style'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Image style'),
      '#maxlength' => 64,
      '#required' => true,
      '#default_value' => isset($slider_type->stid) ? $slider_type->image_style : "slide",
    );

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('System name'),
      '#maxlength' => 32,
      '#required' => true,
      '#default_value' => isset($slider_type->stid) ? $slider_type->name : "",
    );

    $form['speed'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Scrolling speed slides'),
      '#maxlength' => 5,
      '#required' => true,
      '#default_value' => isset($slider_type->stid) ? $slider_type->speed : "3000",
      '#description' => $this->t('The value must be in milliseconds.'),
    );

    $form['type'] = array(
      '#type' => 'select',
      '#title' => $this->t('Type of plugin'),
      '#options' => ['flexslider' => 'flexslider'],
      '#required' => true,
      '#default_value' => isset($slider_type->stid) ? $slider_type->type : "",
    );

    $renderData = array(
      '#theme' => 'site_sliders_weight',
      '#slider_type' => $slider_type,
      '#attached' => array(
        'library' => array(
          'site_sliders/site_sliders_admin',
        ),
      ),
    );
    $sliders = \Drupal::service('renderer')->render($renderData, FALSE);
    $form['sliders'] = array(
      '#type' => 'item',
      '#title' => $this->t('Changing the image of weight'),
      '#markup' => $sliders,
    );

    // Now we add our submit button, for submitting the form results.
    // The 'actions' wrapper used here isn't strictly necessary for tabledrag,
    // but is included as a Form API recommended practice.
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $slider_type = $form_state->getValue('slider_type');

    // Выполняем если создаётся новый раздел курс.
    $return_created = null;
    $return_updated = null;
    if (!isset($slider_type->stid)) {
      $uuid = \Drupal::service('uuid');
      $data = array(
        'uuid' => $uuid->generate(),
        'name' => $form_state->getValue('name'),
        'speed' => $form_state->getValue('speed'),
        'image_style' => $form_state->getValue('image_style'),
        'type' => $form_state->getValue('type'),
        'title' => $form_state->getValue('title'),
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
      );
      $return_created = $this->databaseSliders->insertSliderType($data);
      $slider_type = $this->databaseSliders->loadSliderType($return_created);
    } else {
      // Выполняем если обновляется раздел курса.
      $entry = array(
        'stid' => $slider_type->stid,
        'title' => $form_state->getValue('title'),
        'speed' => $form_state->getValue('speed'),
        'image_style' => $form_state->getValue('image_style'),
        'changed' => REQUEST_TIME,
      );
      $return_updated = $this->databaseSliders->updateSliderType($entry);
    }

    if ($return_created) {
      drupal_set_message($this->t('Slider type «@title» created.', array('@title' => $form_state->getValue('title'))));
    }

    if ($return_updated) {
      drupal_set_message($this->t('Slider type «@title» updated.', array('@title' => $form_state->getValue('title'))));
    }

    // Очищает cache.
    Cache::invalidateTags(['sliders']);
  }
}