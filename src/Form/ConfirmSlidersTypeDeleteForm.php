<?php

/**
 * @file
 * Contains \Drupal\site_kadry_courses\Form\ConfirmStepDeleteForm.
 */

namespace Drupal\site_kadry_courses\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\site_kadry_courses\Controller\CoursesDatabaseController;
use Drupal\site_organization\Controller\SiteOrganizationDatabaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to unban IP addresses.
 */
class ConfirmStepDeleteForm extends ConfirmFormBase {

  /**
   * The ID of the parametrs.
   *
   * @var integral
   */
  protected $course_id;
  protected $course_sid;
  protected $course_eid;
  protected $course_esid;

  /**
   * The object to delete.
   *
   * @var object
   */
  protected $step;

  /**
   * The servises classes.
   *
   * @var \Drupal\site_kadry_courses\Controller\CoursesDatabaseController
   * @var \Drupal\site_organization\Controller\SiteOrganizationDatabaseController
   */
  protected $databaseCourses;
  protected $databaseOrganization;

  /**
   * Constructs a new DblogClearLogForm.
   *
   * @param \Drupal\site_kadry_courses\Controller\CoursesDatabaseController $connection
   *   The database connection.
   */
  public function __construct(CoursesDatabaseController $databaseCourses, SiteOrganizationDatabaseController $databaseOrganization) {
    $this->databaseCourses = $databaseCourses;
    $this->databaseOrganizations = $databaseOrganization;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_kadry_courses.database'),
      $container->get('site_organization.database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'step_delete_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param string $ban_id
   *   The IP address record ID to unban.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $course_esid = NULL) {
    $this->course_esid = (int) $course_esid;
    if ($this->course_esid) {
      $step = (object) $this->databaseCourses->loadStep($this->course_esid);
      $this->step = $step;
      $this->course_id = $step->course_id;
      $this->course_sid = $step->course_sid;
      $this->course_eid = $step->course_eid;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete «@title»?', array('@title' => $this->step->title));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('site_kadry_courses.view_exercise', array('course_id' => $this->course_id, 'course_sid' => $this->course_sid, 'course_eid' => $this->course_eid, 'course_esid' => $this->course_esid));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->databaseCourses->deleteStep($this->course_esid);

    // Очищает cache.
    Cache::invalidateTags(['exercise:' . $this->course_eid]);

    drupal_set_message($this->t('The step was deleted.'));
    $form_state->setRedirect('site_kadry_courses.view_exercise', array('course_id' => $this->course_id, 'course_sid' => $this->course_sid, 'course_eid' => $this->course_eid, 'course_esid' => $this->course_esid));
  }
}