/**
 * @file
 * Misc JQuery scripts in this file
 */
(function($, Drupal, drupalSettings) {
  "use strict";

  Drupal.behaviors.sortableAjaxFunctions = {
    attach: function(context, settings) {
      $(".b-sliders-weight").sortable({
        placeholder: "ui-state-highlight",
        update: function(event, ui) {
          var stid = parseInt($("#" + event.target.id).attr("stid"));
          var sorted = $("#" + event.target.id).sortable("serialize", { key: "sid" });

          // Выполняет запрос ajax.
          var ajaxObject = Drupal.ajax({
            url: "/admin/config/kvantstudio/sliders/slides-set-weight/" + stid + "/&" + sorted + "/nojs",
            base: false,
            element: false,
            progress: false
          });
          ajaxObject.execute();
        }
      });
      $(".b-sliders-weight").disableSelection();
    }
  };
})(jQuery, Drupal, drupalSettings);
