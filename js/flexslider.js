/**
 * @file
 * Misc JQuery scripts in this file
 */
(function($, Drupal, drupalSettings) {
  "use strict";

  $(document).ready(function() {
    $(".flexslider").flexslider({
      animation: "fade",
      controlNav: false,
      directionNav: true,
      prevText: "Назад",
      nextText: "Далее",
      slideshowSpeed: drupalSettings.slide_speed
    });
  });
})(jQuery, Drupal, drupalSettings);
